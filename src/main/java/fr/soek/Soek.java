package fr.soek;

import fr.soek.controller.HomeController;
import fr.soek.controller.SearchController;
import fr.soek.controller.StageManager;
import fr.soek.model.search.SearchFactory;
import javafx.application.Application;
import javafx.stage.Stage;

public class Soek extends Application {

    public static Stage myStage;

    @Override
    public void start(Stage myStage) throws Exception {

        new StageManager(myStage);

        new HomeController();

    }

    public static void main(String[] args) {
        launch(args);
    }
}
