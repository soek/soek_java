// 
// Decompiled by Procyon v0.5.36
// 

package fr.soek.model;

import fr.soek.model.exception.NotIndexedException;
import fr.soek.model.exception.SoekNotFoundException;
import fr.soek.model.result.Result;
import fr.soek.model.result.ResultFactory;

import java.io.*;
import java.util.*;

public class SerialC{
    private String soekPath;

    private static SerialC instance = new SerialC();

    public static SerialC getInstance(){
        return instance;
    }

    public SerialC(){
        this.soekPath="soek_c/soek";
    }

    public void setSoekPath(String soekPath) {
        this.soekPath = soekPath;
    }

    public List<Result> searchWord(final String word) throws IOException, FileNotFoundException {
        try {
            final Process p = Runtime.getRuntime().exec( soekPath + " chercher " + word);
            try {
                p.waitFor();
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        catch (IOException e2) {
            throw new SoekNotFoundException();
        }
        final List<Result> listeResultat = lireResultat();
        try {
            Runtime.getRuntime().exec("rm data/resultat");
        }
        catch (IOException e2) {
            e2.printStackTrace();
        }
        return listeResultat;
    }
    
    public List<Result> searchByFile(final String filePathCompare) throws FileNotFoundException {
        final File fileCompare = new File(filePathCompare);
        if (!fileCompare.isFile()) {
            throw new FileNotFoundException();
        }
        try {
            final Process p = Runtime.getRuntime().exec( soekPath + " comparer " + filePathCompare);
            try {
                p.waitFor();
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        catch (IOException e2) {
            throw new SoekNotFoundException();
        }
        final List<Result> listeResultat = lireResultat();
        try {
            Runtime.getRuntime().exec("rm data/resultat");
        }
        catch (IOException e2) {
            e2.printStackTrace();
        }
        return listeResultat;
    }
    
    private List<Result> lireResultat() throws FileNotFoundException {
        final List<Result> listeResultat = new ArrayList<Result>();
        final File fichierResultat = new File("data/resultat");
        if(!fichierResultat.isFile())
            return new ArrayList<>();
        final Scanner lecteur = new Scanner(fichierResultat);
        while (lecteur.hasNextLine()) {
            final String ligneEnCours = lecteur.nextLine();
            final String[] ligneSplit = ligneEnCours.split(" ");
            if (ligneSplit.length == 2) {
                try {
                    final String filePath = ligneSplit[0];
                    final int distance = Integer.parseInt(ligneSplit[1]);
//                    System.out.println(filePath+ligneSplit);
                    listeResultat.add(ResultFactory.createResult(filePath, distance));
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        lecteur.close();
        return listeResultat;
    }
    
    public String getDescripteur(final String filePath) throws NotIndexedException, SoekNotFoundException, FileNotFoundException {
        String descripteur = "";
        try {
            final Process p = Runtime.getRuntime().exec( soekPath + " descripteur " + filePath);
            try {
                p.waitFor();
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        catch (IOException e2) {
            throw new SoekNotFoundException();
        }
        descripteur = lireDescripteur();
        try {
            Runtime.getRuntime().exec("rm data/descripteur");
        }
        catch (IOException ex) {}
        return descripteur;
    }
    
    private String lireDescripteur() throws NotIndexedException, FileNotFoundException {
        String descripteur = "";
        final File fichierDescripteur = new File("data/descripteur");
        final Scanner lecteur = new Scanner(fichierDescripteur);
        if (lecteur.hasNextLine()) {
            final String ligneEnCours = lecteur.nextLine();
            if (ligneEnCours.equals("Erreur")) {
                lecteur.close();
                throw new NotIndexedException();
            }
            descripteur = ligneEnCours;
        }
        while (lecteur.hasNextLine()) {
            descripteur = String.valueOf(descripteur) + "\n" + lecteur.nextLine();
        }
        lecteur.close();
        return descripteur;
    }

    public void indexer(final String filePath) throws SoekNotFoundException {
        Process p = null;
        try {
            p = Runtime.getRuntime().exec( soekPath + " indexer " + filePath);
        }
        catch (IOException e2) {
            throw new SoekNotFoundException();
        }
        try {
            p.waitFor();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public Map<SettingsManager.Settings, Integer> loadSettings() throws SoekNotFoundException, FileNotFoundException {
        Process p = null;
        try {
            p = Runtime.getRuntime().exec(soekPath);
        }
        catch (IOException e2) {
            throw new SoekNotFoundException();
        }
        try {
            p.waitFor();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        final File soekConfig = new File("data/soek.config");
        if (!soekConfig.isFile()) {
            throw new FileNotFoundException();
        }
        return this.readSettings(soekConfig);
    }

    private Map<SettingsManager.Settings, Integer> readSettings(final File soekConfig) throws FileNotFoundException {
        final Map<SettingsManager.Settings, Integer> mapParametres = new HashMap<SettingsManager.Settings, Integer>();
        final List<SettingsManager.Settings> nomParametres = new ArrayList<SettingsManager.Settings>() {
            private static final long serialVersionUID = 1L;

            {
                this.add(SettingsManager.Settings.AUDIO_DISTANCE_MAX);
                this.add(SettingsManager.Settings.BW_DISTANCE_MAX);
                this.add(SettingsManager.Settings.NB_RESULT);
                this.add(SettingsManager.Settings.RGB_DISTANCE_MAX);
                this.add(SettingsManager.Settings.TEXT_DISTANCE_MAX);
            }
        };
        final Scanner lecteur = new Scanner(soekConfig);
        while (lecteur.hasNextLine()) {
            final String ligneEnCours = lecteur.nextLine();
            final String[] ligneSplit = ligneEnCours.split(" : ");
            for (final SettingsManager.Settings nomParametre : nomParametres) {
                final String nom = nomParametre.getText();
                if (nom.equals(ligneSplit[0])) {
                    mapParametres.remove(nomParametre);
                    final int valeurParametre = Integer.parseInt(ligneSplit[1]);
                    mapParametres.put(nomParametre, valeurParametre);
                }
            }
        }
        lecteur.close();
        return mapParametres;
    }

    public void saveSettings(Map<SettingsManager.Settings, Integer> settingMap ) throws FileNotFoundException, SoekNotFoundException {
        final Set<SettingsManager.Settings> setNomParametre = settingMap.keySet();
        final Set<SettingsManager.Settings> treeSetNomParametre = new TreeSet<>(setNomParametre);
        Process p = null;
        try {
            p = Runtime.getRuntime().exec(soekPath);
        }
        catch (IOException e2) {
            throw new SoekNotFoundException();
        }
        try {
            p.waitFor();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        FileWriter writer;
        try {
            writer = new FileWriter("data/soek.config");
        }
        catch (IOException e2) {
            throw new FileNotFoundException();
        }
        final BufferedWriter bufferedWitter = new BufferedWriter(writer);
        try {
            for (final SettingsManager.Settings nomParametre : treeSetNomParametre) {
                final String nomEnregistrement = nomParametre.getText();
                final int valeur = settingMap.get(nomParametre);
                bufferedWitter.write(nomEnregistrement + " : " + valeur + "\n");
            }
            bufferedWitter.close();
        }
        catch (IOException e2) {
            e2.printStackTrace();
        }
    }

}
