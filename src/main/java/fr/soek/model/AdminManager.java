package fr.soek.model;

public class AdminManager {

    private Boolean admin=false;

    private static AdminManager instance=new AdminManager();

    private AdminManager(){}

    public static AdminManager getInstance() {
        return instance;
    }

    public Boolean getAdmin(){
        return admin;
    }

    public void setAdmin(Boolean admin){
        this.admin = admin;
    }
}
