package fr.soek.model.exception;

public class NotIndexedException extends RuntimeException
{
    private static final long serialVersionUID = 1L;
    
    public NotIndexedException() {
    }
    
    public NotIndexedException(final String message) {
        super(message);
    }
    
    public NotIndexedException(final Throwable cause) {
        super(cause);
    }
    
    public NotIndexedException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
