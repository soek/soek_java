package fr.soek.model.exception;

public class SoekNotFoundException extends RuntimeException
{
    private static final long serialVersionUID = 1L;
    
    public SoekNotFoundException() {
    }
    
    public SoekNotFoundException(final String message) {
        super(message);
    }
    
    public SoekNotFoundException(final Throwable cause) {
        super(cause);
    }
    
    public SoekNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
