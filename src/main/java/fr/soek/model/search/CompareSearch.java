package fr.soek.model.search;

import fr.soek.model.SerialC;
import fr.soek.model.result.Result;

import java.io.FileNotFoundException;

public class CompareSearch extends Search{


    private final static SerialC serialC = SerialC.getInstance();

    private Result searchedFile;

    public CompareSearch(Result searchedFile) throws FileNotFoundException {
        super( serialC.searchByFile(searchedFile.getFilePath()) );
        this.searchedFile=searchedFile;
    }

    @Override
    public String getName() {
        return searchedFile.getName();
    }

    public Result getSearchedFile() {
        return searchedFile;
    }

    @Override
    public String toString() {
        return "CompareSearch [name=" + getName() + ", resultList=" + getResultList() + "]";
    }
}
