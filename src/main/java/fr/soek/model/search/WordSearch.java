package fr.soek.model.search;

import fr.soek.model.SerialC;

import java.io.IOException;

public class WordSearch extends Search{

    private final static SerialC serialC = SerialC.getInstance();

    private String word;

    public WordSearch( String word) throws IOException {
        super(serialC.searchWord( word ));
        this.word=word;
    }

    @Override
    public String getName() {
        return word;
    }

    @Override
    public String toString() {
        return "WordSearch [name=" + getName() + ", resultList=" + getResultList() + "]";
    }
}
