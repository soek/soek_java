package fr.soek.model.search;

import fr.soek.model.result.Result;

import java.util.List;

public abstract class Search {

    private List<Result> resultList;

    public Search( List<Result> resultList ){
        this.resultList=resultList;
    }

    public abstract String getName();

    public List<Result> getResultList() {
        return resultList;
    }

    @Override
    public String toString() {
        return "Search [name=" + getName() + ", resultList=" + getResultList() + "]";
    }
}
