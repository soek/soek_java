package fr.soek.model.search;

import java.util.Stack;

public class SearchHistory extends Stack<Search> {

    private static SearchHistory instance = new SearchHistory();

    private SearchHistory(){
        super();
    }

    public void clear(){
        instance = new SearchHistory();
    }

    public static SearchHistory getInstance(){
        return instance;
    }
}
