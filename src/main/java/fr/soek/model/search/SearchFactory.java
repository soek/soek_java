package fr.soek.model.search;

import fr.soek.model.exception.UnknowFileTypeException;
import fr.soek.model.result.ImageResult;
import fr.soek.model.result.Result;
import fr.soek.model.result.ResultFactory;
import fr.soek.model.result.TextResult;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class SearchFactory {

    public static Search createSearchByRequest( String request ) throws IOException {

        File file = new File(request);

        if( file.isFile() ){
            try {
                return new CompareSearch(ResultFactory.createResult(request,0));
            } catch (UnknowFileTypeException e) {
                e.printStackTrace();
            }
        }

        if(request.split(" ").length==1) {
            return new WordSearch(request);
        }
        else{
            return new Search(cutWordByDelimiters(request)) {
                @Override
                public String getName() {
                    return request;
                }
            };
        }
    }

    private static List<Result> cutWordByDelimiters(String request) throws IOException {
        List<String> wordListPlus = new ArrayList<>();
        List<String> wordListMinus = new ArrayList<>();

        StringTokenizer st = new StringTokenizer(request);
        while(st.hasMoreTokens()){
            String currentWord = st.nextToken();

            if((currentWord.contains("+") && !currentWord.contains("-"))||(!currentWord.contains("+") && !currentWord.contains("-"))){
                StringTokenizer word = new StringTokenizer(currentWord,"+");
                while(word.hasMoreTokens()){
                    wordListPlus.add(word.nextToken());
                }
            }
            else if(currentWord.contains("-") && !currentWord.contains("+")){
                StringTokenizer word = new StringTokenizer(currentWord,"-");
                while(word.hasMoreTokens()){
                    wordListMinus.add(word.nextToken());
                }
            }
        }

        return generateResultList(wordListPlus,wordListMinus);
    }

    private static List<Result> concatenatePlus(String word1, String word2) throws IOException{
        List<Result> resultList = new ArrayList<>();
        for (Result i:new WordSearch(word1).getResultList()){
            for (Result j:new WordSearch(word2).getResultList()){
                if (i.getFilePath().equals(j.getFilePath()))
                    resultList.add(new TextResult(i.getFilePath(), (i.getDistance() + j.getDistance()) / 2));
            }
        }
        return resultList;
    }

    private static List<Result> concatenatePlus(List<Result> resultListIn, String word2,List<Result> resultListOut) throws IOException{
        resultListOut.clear();
        for (Result i:resultListIn){
            for (Result j:new WordSearch(word2).getResultList()){
                if (i.getFilePath().equals(j.getFilePath()))
                    resultListOut.add(new TextResult(i.getFilePath(), (i.getDistance() + j.getDistance()) / 2));
            }
        }
        return resultListOut;
    }

    private static List<Result> concatenateMinus(String word1, String word2) throws IOException{
        boolean equals = true;
        List<Result> resultList = new ArrayList<>();
        for (Result i:new WordSearch(word1).getResultList()){
            for (Result j:new WordSearch(word2).getResultList()){
                if (i.getFilePath().equals(j.getFilePath())) {
                    equals = false;
                    break;
                }
            }
            if (equals){
                resultList.add(new TextResult(i.getFilePath(), i.getDistance()));
            }
            equals = true;
        }
        return resultList;
    }

    private static List<Result> concatenateMinus(List<Result> resultListIn, String word2,List<Result> resultListOut) throws IOException{
        boolean equals = true;
        resultListOut.clear();
        for (Result i:resultListIn){
            for (Result j:new WordSearch(word2).getResultList()){
                if (i.getFilePath().equals(j.getFilePath())) {
                    equals = false;
                    break;
                }
            }
            if (equals){
                resultListOut.add(new TextResult(i.getFilePath(), i.getDistance()));
            }
            equals = true;
        }
        return resultListOut;
    }

    private static List<Result> generateResultList(List<String> wordListPlus, List<String> wordListMinus) throws IOException {
        List<Result> resultList = new ArrayList<>();
        int sizeListPlus = wordListPlus.size();
        int sizeListMinus = wordListMinus.size();

        if (sizeListPlus==1 && sizeListMinus==0){
            return new WordSearch(wordListPlus.get(0)).getResultList();
        }
        if (sizeListPlus==0 && sizeListMinus>=1){
            return resultList;
        }
        if(sizeListPlus==1 && sizeListMinus==1){
            return concatenateMinus(wordListPlus.get(0),wordListMinus.get(0));
        }
        if(sizeListPlus==2 && sizeListMinus==0){
            return concatenatePlus(wordListPlus.get(0),wordListPlus.get(1));
        }
        if(sizeListPlus==0 && sizeListMinus==0){
            return resultList;
        }
        resultList = concatenatePlus(wordListPlus.get(0),wordListPlus.get(1));
        for (int i =0;i<sizeListPlus-2;i++){
            resultList=concatenatePlus(resultList,wordListPlus.get(i+2),resultList);
        }
        for (int i=0;i<sizeListMinus;i++){
            resultList=concatenateMinus(resultList,wordListMinus.get(i),resultList);
        }
        return resultList;
    }
}
