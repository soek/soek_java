package fr.soek.model;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Set;

import java.util.Map;

public class SettingsManager {
    private Map<Settings, Integer> savedSettingsMap;
    private Map<Settings, Integer> settingsMap;

    private static SettingsManager instance = null;

    public static SettingsManager getInstance() {
        if(instance==null)
            instance = new SettingsManager();
        return SettingsManager.instance;
    }

    private SettingsManager(){
        try {
            savedSettingsMap = SerialC.getInstance().loadSettings();
            settingsMap = new HashMap<>(savedSettingsMap);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public int getNumberResult() {
        Object numberResult = settingsMap.get(Settings.NB_RESULT);
        return (numberResult!=null)? (int) numberResult : 10 ;
    }

    public void setNumberResult(int numberResult) {
        settingsMap.put( Settings.NB_RESULT, numberResult );
    }


    public int getImageDistance() {
        Object imageDistance = settingsMap.get(Settings.BW_DISTANCE_MAX);
        return (imageDistance!=null)? (int) imageDistance : 30000 ;
    }

    public void setImageDistance(int imageDistance) {
        settingsMap.put( Settings.BW_DISTANCE_MAX, imageDistance );
        settingsMap.put( Settings.RGB_DISTANCE_MAX, imageDistance );
    }

    public int getAudioDistance() {
        Object audioDistance = settingsMap.get(Settings.AUDIO_DISTANCE_MAX);
        return (audioDistance!=null)? (int) audioDistance : 30000 ;
    }

    public void setAudioDistance(int audioDistance) {
        settingsMap.put( Settings.AUDIO_DISTANCE_MAX, audioDistance );
    }

    public int getTextDistance() {
        Object textDistance = settingsMap.get(Settings.TEXT_DISTANCE_MAX);
        return (textDistance!=null)? (int) textDistance : 5000 ;
    }

    public void setTextDistance(int textDistance) {
        settingsMap.put( Settings.TEXT_DISTANCE_MAX, textDistance );
    }


    public void cancel() {
        settingsMap = new HashMap<>(savedSettingsMap);
    }

    public void save(){
        try {
            SerialC.getInstance().saveSettings(settingsMap);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        savedSettingsMap = new HashMap<>(settingsMap);
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("Parametre { ParametreSave= ");
        s.append(savedSettingsMap);
        s.append(settingsMap);
        s.append("}");
        return s.toString();
    }



    enum Settings
    {
        NB_RESULT("Result number"),
        TEXT_DISTANCE_MAX("Text maximum distance"),
        AUDIO_DISTANCE_MAX("audio maximum distance"),
        RGB_DISTANCE_MAX("RGB maximum distance"),
        BW_DISTANCE_MAX("Black and white maximum distance");

        private String text;
        Settings( String text ){
            this.text = text;
        }

        public String getText() {
            return text;
        }
    }

}















