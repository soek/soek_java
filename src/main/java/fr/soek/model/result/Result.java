package fr.soek.model.result;


import fr.soek.model.SerialC;

import java.io.FileNotFoundException;

public abstract class Result implements Comparable<Result>{
    private String filePath;
    private int distance;
    
    public Result(final String filePath, final int distance) {
        this.filePath = filePath;
        this.distance = distance;
    }
    
    @Override
    public boolean equals(final Object object) {
        if (object instanceof Result) {
            final Result resultat = (Result)object;
            return this.distance == resultat.distance && this.filePath.equals(resultat.filePath);
        }
        return false;
    }
    
    public int compareTo(final Result resultat) {
        final int distance = this.distance - resultat.distance;
        if (distance != 0) {
            return distance;
        }
        return this.filePath.compareTo(resultat.filePath);
    }
    
    public String getFilePath() {
        return this.filePath;
    }

    public String getName() {
        return filePath.substring(filePath.lastIndexOf("/")+1, filePath.lastIndexOf("."));
    }

    public  String getLocation(){
        return  filePath.substring(0,filePath.lastIndexOf("/"));
    }
    
    public int getDistance() {
        return this.distance;
    }

    public String getDscptr(){
        try {
            return SerialC.getInstance().getDescripteur(this.filePath);
        }catch (FileNotFoundException e){
            e.getStackTrace();
            return this.filePath+"\n ERROR: Descripteur absent";
        }

    }
}
