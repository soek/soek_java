package fr.soek.model.result;

public class AudioResult extends Result {
    public AudioResult(String filePath, int distance) {
        super(filePath, distance);
    }

    public AudioResult(String filePath ){
        super( filePath, 0 );
    }


    @Override
    public String toString() {
        return "AudioResultat [filePath=" + getFilePath() + ", distance=" + getDistance() + ", icon=" + "fr/soek/view/images/note.png" + "]";
    }
}
