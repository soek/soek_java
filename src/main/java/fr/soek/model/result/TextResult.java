package fr.soek.model.result;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class TextResult extends Result {
    public TextResult(String filePath, int distance) {
        super( filePath, distance );
    }

    public TextResult(String filePath ){
        super( filePath, 0 );
    }

    @Override
    public String toString() {
        return "TextResultat [filePath=" + getFilePath() + ", distance=" + getDistance() + ", icon=" + "fr/soek/view/images/texte.png" + "]";
    }

    public String getText() {
        String         line = null;
        StringBuilder  stringBuilder = new StringBuilder();
        String         ls = System.getProperty("line.separator");
        boolean inTag=false;
        try{
            BufferedReader reader = new BufferedReader(new FileReader( new File(this.getFilePath())));

            try {
                while(((line = reader.readLine()) != null)) {

                    for( char letter : line.toCharArray()){
                        if( letter=='<')
                            inTag= true;
                        if(!inTag)
                            stringBuilder.append(letter);
                        if( letter=='>')
                            inTag= false;
                    }
                    stringBuilder.append(ls);
                }

            } finally {
                reader.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }
}
