package fr.soek.model.result;

public class ImageResult extends Result {
    public ImageResult(String filePath, int distance) {
        super( filePath, distance );
    }

    public ImageResult( String filePath ){
        super( filePath, 0 );
    }


    @Override
    public String toString() {
        return "ImageResultat [filePath=" + getFilePath() + ", distance=" + getDistance() + ", icon=" + getFilePath() + "]";
    }
}
