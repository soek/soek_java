package fr.soek.model.result;

import fr.soek.model.exception.UnknowFileTypeException;

public class ResultFactory {
    public static Result createResult( String filePath, int distance) throws UnknowFileTypeException {
        final String[] filePathSplit = filePath.split("\\.");
        final String extension = filePathSplit[filePathSplit.length - 1];
        switch (extension) {
            case "wav":
                return new AudioResult( filePath, distance );
            case "bmp":
            case "jpg":
                return new ImageResult( filePath, distance );
            case "xml":
                return new TextResult( filePath, distance );
        }
        throw new UnknowFileTypeException();
    }

}
