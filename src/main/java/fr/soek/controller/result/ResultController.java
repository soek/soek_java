package fr.soek.controller.result;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;

import java.io.IOException;

public abstract class ResultController {

    public abstract void dispose();
    protected FXMLLoader loader;
    public Node getNode(){
        try {
            return loader.load();
        } catch (IOException ignored) {}
        return null;
    }
}
