package fr.soek.controller.result;

import fr.soek.model.result.AudioResult;
import javafx.beans.Observable;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.ImageView;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class AudioResultController extends ResultController implements Initializable {

    private AudioResult audioResult;
    private MediaPlayer mediaPlayer;

    private boolean isPlaying;

    @FXML Button playButton;
    @FXML Button stopButton;


    @FXML
    private ImageView previewImageView;
    @FXML
    private Label fileNameLabel;
    @FXML
    private Slider slider;

    public AudioResultController(AudioResult audioResult ){
        this.audioResult =audioResult;
        loader = new FXMLLoader(getClass().getResource("/fr/soek/view/audioResult.fxml"));
        loader.setController(this);
        String str = (new File(audioResult.getFilePath())).toURI().toString();
        mediaPlayer = new MediaPlayer( new Media(str) );

        isPlaying = false;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        fileNameLabel.setText( audioResult.getFilePath());

        slider.setMax(mediaPlayer.getTotalDuration().toSeconds());
        slider.valueProperty().addListener(ov -> {
                    if (slider.isValueChanging()) {
                        mediaPlayer.seek(Duration.seconds(slider.getValue()));
                    }
                });

        mediaPlayer.currentTimeProperty().addListener((Observable ov) -> {
            slider.adjustValue(mediaPlayer.getCurrentTime().toSeconds());
            slider.setMax(mediaPlayer.getTotalDuration().toSeconds());
        });
    }

    @FXML
    public void play(){
        isPlaying = !isPlaying;

        if(isPlaying){
            mediaPlayer.play();
            playButton.setText("Pause");
            stopButton.setDisable(false);
        }
        else{
            mediaPlayer.pause();
            playButton.setText("Play");
            stopButton.setDisable(true);
        }
    }

    @FXML
    public void stop(){
        mediaPlayer.stop();
        playButton.setText("Play");
        stopButton.setDisable(true);
    }

    @Override
    public void dispose() {
        this.stop();
    }
}