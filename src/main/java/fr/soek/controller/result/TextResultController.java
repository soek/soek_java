package fr.soek.controller.result;

import fr.soek.model.result.TextResult;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class TextResultController extends ResultController  implements Initializable{

    private TextResult textResult;


    @FXML
    private Label fileNameLabel;
    @FXML
    private TextArea textPreview;

    public TextResultController(TextResult textResult ){
        this.textResult =textResult;
        loader = new FXMLLoader(getClass().getResource("/fr/soek/view/textResult.fxml"));
        loader.setController(this);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        fileNameLabel.setText( textResult.getFilePath());
        textPreview.setText( textResult.getText() );
    }

    @Override
    public void dispose() {
    }
}
