package fr.soek.controller.result;

import fr.soek.model.result.ImageResult;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ImageResultController  extends ResultController implements Initializable {

    private ImageResult imageResult;


    @FXML
    private ImageView previewImageView;
    @FXML
    private Label fileNameLabel;

    public ImageResultController( ImageResult imageResult ){
        this.imageResult=imageResult;
        loader = new FXMLLoader(getClass().getResource("/fr/soek/view/imageResult.fxml"));
        loader.setController(this);
    }

    @Override
    public void dispose() {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            FileInputStream inputstream = new FileInputStream(imageResult.getFilePath());
            previewImageView.setImage( new Image(inputstream) );
        } catch (FileNotFoundException e) {
            e.getStackTrace();
        }
        fileNameLabel.setText( imageResult.getFilePath());
    }
}
