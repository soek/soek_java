package fr.soek.controller;

import fr.soek.model.AdminManager;
import fr.soek.model.result.AudioResult;
import fr.soek.model.result.ImageResult;
import fr.soek.model.result.Result;
import fr.soek.model.search.CompareSearch;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ResultCell extends ListCell<Result> {

    @FXML
    private ImageView imageView;

    @FXML
    private Label title;

    @FXML
    private HBox pane;

    @FXML
    private ImageView imageViewDirectory;

    @FXML
    private ImageView imageViewLens;

    @FXML
    private ImageView imageViewEye;

    private FXMLLoader mLLoader;

    private Result result;

    @Override
    protected void updateItem(Result result, boolean empty) {
        super.updateItem(result, empty);

        this.result = result;
        if(empty || result == null) {

            setText(null);
            setGraphic(null);

        } else {
            if (mLLoader == null) {
                mLLoader = new FXMLLoader(getClass().getResource("/fr/soek/view/listCellItem.fxml"));
                mLLoader.setController(this);

                try {
                    mLLoader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            title.setText( result.getName() );

            Image image = new Image("fr/soek/view/images/01.jpg");
            if(result instanceof ImageResult){
                try {
                    FileInputStream inputstream = new FileInputStream(result.getFilePath());
                    image = new Image(inputstream);
                } catch (FileNotFoundException e) {
                    e.getStackTrace();
                }
            } else if(result instanceof AudioResult){
                image = new Image("fr/soek/view/images/note.png");
            } else {
                image = new Image("fr/soek/view/images/texte.png");
            }

            imageView.setImage(image);
            imageViewEye.setVisible(AdminManager.getInstance().getAdmin());
            setText(null);
            setGraphic(pane);
        }
    }

    @FXML
    public void searchBy() throws FileNotFoundException {
        new SearchController(new CompareSearch(result));
    }

    public void printDscptr(){
        String dscptr = result.getDscptr();
        Dialog dialog = new Dialog();
        dialog.setTitle(result.getName());
        dialog.setContentText(dscptr);
        ButtonType loginButtonType = new ButtonType("Fermer", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().add(loginButtonType);
        dialog.show();
    }

    public void fiendFolder() throws IOException {
        Runtime.getRuntime().exec("xdg-open "+result.getLocation());
    }


}