package fr.soek.controller;

import fr.soek.model.SettingsManager;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

import java.net.URL;
import java.util.ResourceBundle;

public class SettingsController implements Initializable {

    public SettingsManager settingsManager;

    @FXML
    private Slider numberResultSlider;
    @FXML
    private Slider textDistanceSlider;
    @FXML
    private Slider imageDistanceSlider;
    @FXML
    private Slider audioDistanceSlider;
    @FXML
    private TextField numberResultTextField;
    @FXML
    private Button saveButton;

    public SettingsController()  {

        settingsManager = SettingsManager.getInstance();

        try {

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/soek/view/setting.fxml"));
            loader.setController(this);
            Parent pane = (Pane) loader.load();
            Scene scene = new Scene(pane);

            StageManager.getInstance().setScene( scene);

        } catch (Exception e) {
            e.printStackTrace();
        }

        numberResultSlider.valueProperty().addListener(ov -> {
            if (numberResultSlider.isValueChanging()) {
                settingsManager.setNumberResult((int) numberResultSlider.getValue());
                numberResultTextField.setText(String.valueOf(settingsManager.getNumberResult()));
                saveButton.setDisable(false);
            }
        });

        textDistanceSlider.valueProperty().addListener(ov -> {
            if (textDistanceSlider.isValueChanging()) {
                settingsManager.setTextDistance( 10000-(int) textDistanceSlider.getValue());
                saveButton.setDisable(false);
            }
        });

        imageDistanceSlider.valueProperty().addListener(ov -> {
            if (imageDistanceSlider.isValueChanging()) {
                settingsManager.setImageDistance( 50000 - (int) imageDistanceSlider.getValue());
                saveButton.setDisable(false);
            }
        });

        audioDistanceSlider.valueProperty().addListener(ov -> {
            if (audioDistanceSlider.isValueChanging()) {
                settingsManager.setAudioDistance( 50000 - (int) audioDistanceSlider.getValue());
                saveButton.setDisable(false);
            }
        });


    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        numberResultSlider.setValue(settingsManager.getNumberResult());
        textDistanceSlider.setValue( 10000 - settingsManager.getTextDistance());
        imageDistanceSlider.setValue( 50000 - settingsManager.getImageDistance());
        audioDistanceSlider.setValue( 50000 - settingsManager.getAudioDistance());
        numberResultTextField.setText(String.valueOf(settingsManager.getNumberResult()));
    }

    @FXML
    void save(){
        SettingsManager.getInstance().save();
        new HomeController();
    }

    @FXML
    void exit(){
        SettingsManager.getInstance().cancel();
        new HomeController();
    }
}
