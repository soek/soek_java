package fr.soek.controller;

import fr.soek.model.AdminManager;
import fr.soek.model.SerialC;
import fr.soek.model.search.SearchFactory;
import fr.soek.model.search.SearchHistory;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class HomeController implements Initializable {

    @FXML
    private TextField requestTextField;
    @FXML
    private ImageView imageViewAdmin;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Label labelPassword;
    @FXML
    private ImageView imageViewSetting;
    @FXML
    private ImageView imageViewAddFile;
    @FXML
    private Button buttonLogIn;

    public HomeController() {
        SearchHistory.getInstance().clear();
        try {


            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/soek/view/home.fxml"));
            loader.setController(this);
            Parent pane = (Pane) loader.load();

            StageManager.getInstance().setTitle("Accueil - SOEK");
            StageManager.getInstance().setScene( new Scene(pane) );

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        buttonLogIn.setVisible(false);
        imageViewSetting.setVisible(false);
        imageViewAddFile.setVisible(false);
        passwordField.setVisible(false);
        labelPassword.setVisible(false);
        passwordField.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(!(newValue.booleanValue()||buttonLogIn.isFocused()))
                    noDisplayLogIn();
            }
        });


        buttonLogIn.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(!(newValue.booleanValue()|| passwordField.isFocused()))
                    noDisplayLogIn();
            }
        });

        passwordField.setOnKeyReleased(event -> {
            if (event.getCode() == KeyCode.ENTER){
                logIn();
            }
        });

        requestTextField.setOnKeyReleased(event -> {
            if (event.getCode() == KeyCode.ENTER){
                sendRequest();
            }
        });


        displayAdmin();
    }

    private void noDisplayLogIn() {
        labelPassword.setVisible(false);
        passwordField.setVisible(false);
        buttonLogIn.setVisible(false);
    }

    @FXML
    public void sendRequest() {
        if(!requestTextField.getText().isEmpty()){
            try {
                new SearchController(SearchFactory.createSearchByRequest(requestTextField.getText()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    public void displayLogIn(){
        if(!AdminManager.getInstance().getAdmin()){
            labelPassword.setVisible(true);
            passwordField.setVisible(true);
            buttonLogIn.setVisible(true);
            passwordField.requestFocus();
        } else {
            AdminManager.getInstance().setAdmin(false);
            displayAdmin();
        }
    }

    public void displayAdmin(){
        if(AdminManager.getInstance().getAdmin()){
            imageViewAdmin.setImage(new Image("/fr/soek/view/images/logout.png"));
            imageViewSetting.setVisible(true);
            imageViewAddFile.setVisible(true);
            noDisplayLogIn();
        } else {
            imageViewAdmin.setImage(new Image("/fr/soek/view/images/admin.png"));
            imageViewSetting.setVisible(false);
            imageViewAddFile.setVisible(false);
        }
    }

    @FXML
    public void logIn(){
        if(passwordField.getText().equals("pwd")){
            AdminManager.getInstance().setAdmin(true);
            displayAdmin();
        }
    }

    @FXML
    public void addFile(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Ajouter un fichier à la base");
        fileChooser.getExtensionFilters().addAll(
                new ExtensionFilter("Indexable Files", "*.xml", "*.bmp", "*.jpg", "*.wav"),
                new ExtensionFilter("Text Files", "*.xml"),
                new ExtensionFilter("Image Files", "*.bmp", "*.jpg"),
                new ExtensionFilter("Audio Files", "*.wav"),
                new ExtensionFilter("All Files", "*.*"));
        File selectedFile = fileChooser.showOpenDialog(StageManager.getInstance().getStage());
        if( selectedFile!=null ){
            SerialC serialC = SerialC.getInstance();
            serialC.indexer(selectedFile.getPath());
        }
    }

    @FXML
    public void goSettingsMenu(){
        new SettingsController();
    }

    @FXML
    public void findFind(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Ajouter un fichier à la base");
        fileChooser.getExtensionFilters().addAll(
                new ExtensionFilter("Indexable Files", "*.xml", "*.bmp", "*.jpg", "*.wav"),
                new ExtensionFilter("Text Files", "*.xml"),
                new ExtensionFilter("Image Files", "*.bmp", "*.jpg"),
                new ExtensionFilter("Audio Files", "*.wav"),
                new ExtensionFilter("All Files", "*.*"));
        File selectedFile = fileChooser.showOpenDialog(StageManager.getInstance().getStage());
        if( selectedFile!=null ){
            requestTextField.setText( selectedFile.getPath() );
        }
    }
}
