package fr.soek.controller;

import javafx.scene.Scene;
import javafx.stage.Stage;

public class StageManager {

    private static StageManager instance;

    public static StageManager getInstance() {
        return instance;
    }

    private Stage stage;

    public StageManager( Stage stage ){
        this.stage = stage;
        instance = this;
        this.stage.show();
    }

    public void setScene( Scene scene ) {
        this.stage.setScene(scene);
    }

    public void setTitle( String title ){
        this.stage.setTitle(title);
    }

    public Stage getStage() {
        return stage;
    }
}
