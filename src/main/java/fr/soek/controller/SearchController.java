package fr.soek.controller;

import fr.soek.controller.result.AudioResultController;
import fr.soek.controller.result.ImageResultController;
import fr.soek.controller.result.ResultController;
import fr.soek.controller.result.TextResultController;
import fr.soek.model.result.AudioResult;
import fr.soek.model.result.ImageResult;
import fr.soek.model.result.Result;
import fr.soek.model.result.TextResult;
import fr.soek.model.search.CompareSearch;
import fr.soek.model.search.Search;
import fr.soek.model.search.SearchHistory;
import fr.soek.model.search.WordSearch;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.Pane;

import javafx.scene.image.ImageView;
import java.net.URL;
import java.util.ResourceBundle;

public class SearchController implements Initializable {

    @FXML
    private Pane resultPreview;

    @FXML
    private ListView<Result> listView;

    @FXML
    private Label title;

    @FXML
    private ImageView imageViewPrevious;

    private Search search;
    private ResultController resultController;

    private ObservableList<Result> imageObservableList;


    public SearchController( Search search )  {

        this.search = search;
        SearchHistory.getInstance().push(this.search);

        imageObservableList = FXCollections.observableArrayList();
       if(!search.getResultList().isEmpty()){
           imageObservableList.setAll(search.getResultList());
       }

        try {

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/soek/view/search.fxml"));
            loader.setController(this);
            Parent pane = (Pane) loader.load();
            Scene scene = new Scene(pane);

            if(!search.getResultList().isEmpty()){
                StageManager.getInstance().setTitle(search.getName());
            } else {
                StageManager.getInstance().setTitle("Aucun resultat - SOEK");
            }

            StageManager.getInstance().setScene( scene);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if(SearchHistory.getInstance().size() <=1){
            imageViewPrevious.setVisible(false);
        }
        if( search instanceof CompareSearch )
            setDisplayedResult( ((CompareSearch) search).getSearchedFile() );
        if(!search.getResultList().isEmpty()){
            listView.setItems(imageObservableList);
            listView.setCellFactory(studentListView -> new ResultCell());

            if( search instanceof WordSearch )
                setDisplayedResult(search.getResultList().get(0));


            title.setText( search.getName() );
        } else {
            title.setText("Aucun résultat");
        }
        listView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Result>() {
            @Override
            public void changed(ObservableValue<? extends Result> observable, Result oldValue, Result newValue) {
                if(resultController!=null) resultController.dispose();
                setDisplayedResult(search.getResultList().get(listView.getFocusModel().getFocusedIndex()));
            }
        });
    }

    private void setDisplayedResult( Result displayedResult ){
        if( displayedResult instanceof ImageResult ) {
            ImageResultController imageResultController = new ImageResultController((ImageResult) displayedResult);
            resultController = imageResultController;
            resultPreview.getChildren().setAll(imageResultController.getNode());
        }
        else if(displayedResult instanceof TextResult){
            TextResultController textResultController = new TextResultController((TextResult) displayedResult);
            resultController = textResultController;
            resultPreview.getChildren().setAll(textResultController.getNode());
        }
        else if(displayedResult instanceof AudioResult){
            AudioResultController audioResultController = new AudioResultController((AudioResult) displayedResult);
            resultController = audioResultController;
            resultPreview.getChildren().setAll(audioResultController.getNode());
        }
    }

    @FXML
    public void goToHome(){
        if(resultController!=null) resultController.dispose();
        new HomeController();
    }

    @FXML
    public void previous(){
        if(resultController!=null) resultController.dispose();
        SearchHistory.getInstance().pop();
        new SearchController(SearchHistory.getInstance().pop());
    }

}